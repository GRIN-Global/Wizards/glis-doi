﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using DOIManagerWizard.Model;

namespace DOIManagerWizard
{
    /// <summary>
    /// Configuration manages the persistent, secure store of the user configurable 
    /// options user to connect to the GLIS server
    /// </summary>
    public class Configuration
    {
        private const string PROD_GLIS_SERVER = "https://ssl.fao.org/glis/xml/manager";
        private string _configurationFile;
        private string _username;
        private string _password;
        private Uri _serverAddress;
        private bool _loading;

        // Needed for Serialization/Deserialization
        private Configuration()
        {}

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="filename">Name of the file where the configuration is stored</param>
        public Configuration(string filename)
        {
            this._configurationFile = filename;
            Load();
        }

        /// <summary>
        /// Load configuration from storage
        /// </summary>
        protected void Load()
        {
            if (File.Exists(_configurationFile))
            {
                try
                {
                    _loading = true;

                    // Configuration contains sensitive information, so it is encrypted on disk
                    byte[] unencrypted = System.Security.Cryptography.ProtectedData.Unprotect(File.ReadAllBytes(_configurationFile), null, System.Security.Cryptography.DataProtectionScope.CurrentUser);
                    XmlSerializer serializer = new XmlSerializer(this.GetType());

                    Configuration config = (Configuration)serializer.Deserialize(new MemoryStream(unencrypted));

                    if (!string.IsNullOrWhiteSpace(config.Username))
                        this._username = config.Username;

                    if (!string.IsNullOrWhiteSpace(config.Password))
                        this._password = config.Password;

                    if (config.ServerUri != null)
                        this._serverAddress = config.ServerUri;
                }
                catch (CryptographicException)
                {
                    // We are intentionally swallowing this exception. If the source file is malformed, the 
                    // implementation will simply prompt the user to setup the authentication again.
                }
                catch (InvalidOperationException)
                {
                    // We are intentionally swallowing this exception. If the source file is malformed, the 
                    // implementation will simply prompt the user to setup the authentication again.
                }
                finally
                {
                    // We default the server to the production GLIS server if no server is set
                    if (_serverAddress == null)
                        this._serverAddress = new Uri(PROD_GLIS_SERVER);

                    _loading = false;
                }
            }
            else
            {
                this._serverAddress = new Uri(PROD_GLIS_SERVER);
            }
        }

        /// <summary>
        /// Save configuration to disk
        /// </summary>
        protected void Save()
        {
            if (!_loading && !string.IsNullOrWhiteSpace(_configurationFile))
            {
                // Serialize the class to XML
                XmlSerializer serializer = new XmlSerializer(this.GetType());

                // Write it to disk
                var memStream = new MemoryStream();
                serializer.Serialize(memStream, this);
                byte[] protectedData = ProtectedData.Protect(memStream.ToArray(), null, DataProtectionScope.CurrentUser);
                File.WriteAllBytes(_configurationFile, protectedData);
            }
        }

        /// <summary>
        /// Username to the SMTA server
        /// </summary>
        public string Username
        {
            get
            {
                return _username;
            }
            set
            {
                // If the value changes, save the file
                if (string.Compare(value, _username) != 0)
                {
                    _username = value;
                    if (!_loading) Save();
                }
            }
        }

        /// <summary>
        /// Password to the SMTA server
        /// </summary>
        public string Password
        {
            get
            {
                return _password;
            }
            set
            {
                // If the value changes, save the file
                if (string.Compare(value, _password) != 0)
                {
                    _password = value;
                    if (!_loading) Save();
                }
            }
        }

        /// <summary>
        /// SMTA server address
        /// </summary>
        [XmlIgnore]
        public Uri ServerUri
        {
            get
            {
                return _serverAddress;
            }
            set
            {
                // If the value changes, save the file
                if (_serverAddress == null || !_serverAddress.Equals(value))
                {
                    _serverAddress = value;
                    if (!_loading) Save();
                }
            }
        }
        
        /// <summary>
        /// Server address in string format used to support serialization/deserialization
        /// </summary>
        public string ServerAddress
        {
            get
            {
                return _serverAddress?.ToString();
            }
            set
            {
                _serverAddress = new Uri(value);
            }
        }

        /// <summary>
        /// Indicates whether the configuration is set, however this does not mean
        /// that the username/password/server settings are valid
        /// </summary>
        public bool IsConfigured
        {
            get
            {
                return !string.IsNullOrWhiteSpace(_username) &&
                    !string.IsNullOrWhiteSpace(_password) &&
                    _serverAddress != null;
            }
        }

    }
}
