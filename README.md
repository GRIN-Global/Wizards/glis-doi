## **Easy DOI Manager Wizard**

The "Easy DOI Manager" wizard allow the user requests the DOI from GRIN-Global Cuartor-Tool to GLIS system.

## How to install the Easy DOI Manager Wizard for Curator Tool

1. Download the files: "DOIManagerWizard.dll", "Newtonsoft.Json.dll" and "stdole.dll" from [here](https://gitlab.com/GRIN-Global/Wizards/glis-doi/-/tree/master/DOIManagerWizard.Install).
1. Unblock the DLLs files. Right click the DLL file, select properties and check "Unblock"
1. Copy the file "DOIManagerWizard.dll" to "C:\Program Files\GRIN-Global\GRIN-Global Curator Tool\Wizards" folder.
1. Copy the files "Newtonsoft.Json.dll" and "stdole.dll" to "C:\Program Files\GRIN-Global\GRIN-Global Curator Tool" folder.
1. Run Curator Tool.

## Importing the get_GLIS_data_dataview

Don't forget to import the dataview **get_GLIS_data** through GRIN-Global Admin-Tool, the dataview get_GLIS_data is available [here](https://gitlab.com/GRIN-Global/Wizards/glis-doi/-/blob/master/DOIManagerWizard.Install/get_GLIS_data.dataviewxml) 

