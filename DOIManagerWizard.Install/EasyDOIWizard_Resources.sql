BEGIN TRANSACTION

  INSERT INTO app_resource (sys_lang_id,app_name,form_name,app_resource_name, display_member,created_by,created_date,owned_by,owned_date) 
  VALUES (1,'GRINGlobalClientCuratorTool','EasyDOIWizard','Text', 'Easy DOI Wizard v{0}', 48,getdate(),48,getdate())

  INSERT INTO app_resource (sys_lang_id,app_name,form_name,app_resource_name, display_member,created_by,created_date,owned_by,owned_date) 
  VALUES (1,'GRINGlobalClientCuratorTool','EasyDOIConfigForm','Text', 'DOI Configuration', 48,getdate(),48,getdate())

  INSERT INTO app_resource (sys_lang_id,app_name,form_name,app_resource_name, display_member,created_by,created_date,owned_by,owned_date) 
  VALUES (2,'GRINGlobalClientCuratorTool','EasyDOIWizard','Text', 'Asistente de DOI v{0}', 48,getdate(),48,getdate())

  INSERT INTO app_resource (sys_lang_id,app_name,form_name,app_resource_name, display_member,created_by,created_date,owned_by,owned_date) 
  VALUES (2,'GRINGlobalClientCuratorTool','EasyDOIConfigForm','Text', 'Configuración DOI', 48,getdate(),48,getdate())
  
COMMIT TRANSACTION