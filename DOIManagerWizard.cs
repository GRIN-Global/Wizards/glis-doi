﻿using DOIManagerWizard.Model;
using GRINGlobal.Client.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace DOIManagerWizard
{
    /// <summary>
    /// Interface required by the GRINGlobal Curator Tool wizard framework
    /// </summary>
    interface IGRINGlobalDataWizard
    {
        string FormName { get; }
        DataTable ChangedRecords { get; }
        string PKeyName { get; }
    }

    /// <summary>
    /// Primary DOI Wizard dialog
    /// </summary>
    public partial class DOIManagerWizard : Form, IGRINGlobalDataWizard
    {
        private string _originalPKeys;
        private string _orderRequestPKeys;
        private DOIController _controller;
        private DataTable _accessions;
        private DataTable _dataErrors;
        private BackgroundWorker _bw = null;
        private Cursor _origCursor = null;
        private DOIResponse smtaResponse = null;


        #region IGRINGlobalDataWizard
        /// <summary>
        /// Name of the wizard displayed in the CuratorTool
        /// </summary>
        public string FormName
        {
            get
            {
                return DOIResource.WizardName;
            }
        }

        public DataTable ChangedRecords
        {
            get
            {
                return new DataTable();
            }
        }

        /// <summary>
        /// Required key type
        /// </summary>
        public string PKeyName
        {
            get
            {
                return "accession_id";
            }
        }
        #endregion
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="pKeys">Keys of selected Order Request objects</param>
        /// <param name="sharedUtils">Curator Tool utilities</param>
        public DOIManagerWizard(string pKeys, SharedUtils sharedUtils)
        {
            InitializeComponent();
            if (sharedUtils != null)
            {
                this._controller = new DOIController(sharedUtils);

                // Create errors table
                this._dataErrors = new DataTable();
                this._dataErrors.Columns.AddRange(new[] {
                    new DataColumn("accession_id", typeof(int)),
                    new DataColumn("message")
                    });
            }

            _originalPKeys = pKeys;
            // Ignore all pkey tokens except the ACCESSIONID pkeys...
            foreach (string pkeyToken in pKeys.Split(';'))
            {
                if (pkeyToken.Split('=')[0].Trim().ToUpper() == ":ACCESSIONID") _orderRequestPKeys = pkeyToken;
            }

            // Configure background worker
            _bw = new BackgroundWorker();
            _bw.WorkerReportsProgress = true;
            _bw.RunWorkerCompleted += UploadRunWorkerCompleted;
            _bw.ProgressChanged += UploadProgressChanged;
            _bw.DoWork += UploadGLISData;
        }

        /// <summary>
        /// Form load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DOIManagerWizard_Load(object sender, EventArgs e)
        {
            Text = FormName;

            // Verify that the wizard has the required setup to function
            if (!this.VerifySetup())
            {
                MessageBox.Show(DOIResource.InstallationErrorMsg, DOIResource.InstallationError, MessageBoxButtons.OK, MessageBoxIcon.Stop);
                this.Close();
                return;
            }

            pbProgress.Style = ProgressBarStyle.Marquee;
            this.btnTransfer.Enabled = _controller.Configuration.IsConfigured;//The commit button is able just when the controller form configuration is correct

            // Update Controls based on Languages
            if (this.components != null && this.components.Components != null) _controller.SharedUtils.UpdateComponents(this.components.Components, this.Name);
            if (this.Controls != null) _controller.SharedUtils.UpdateControls(this.Controls, this.Name);

            // Load data for passed
            LoadData(_orderRequestPKeys);
        }

        /// <summary>
        /// Launch the configuration dialog
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnConfigure_Click(object sender, EventArgs e)
        {
            this._controller.Configure();
            this.btnTransfer.Enabled = _controller.Configuration.IsConfigured;
        }

        /// <summary>
        /// Close the wizard
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Initiate the transfer of GLIS records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnTransfer_Click(object sender, EventArgs e)
        {
            // Protect against running twice at once
            if (_bw.IsBusy)
                return;

            // Prepare the UI
            btnTransfer.Enabled = false;
            btnConfigure.Enabled = false;
            SetStatusDetails();
            dtgAccessions.ClearSelection();
            lblUploading.Visible = true;
            pbProgress.Visible = true;
            lblRecordCount.Text = string.Format(DOIResource.Records, 0, this._accessions.DefaultView.Count);
            lblErrorCount.Text = string.Format(DOIResource.Errors, 0);
            lblErrorCount.ForeColor = SystemColors.ControlText;

            // Change cursor to the wait cursor...
            _origCursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;

            // Start the background worker
            _bw.RunWorkerAsync();
        }
        /// <summary>
        /// Primary worker function to upload data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UploadGLISData(object sender, DoWorkEventArgs e)
        {
            int totalRecords = _accessions.Rows.Count;
            int recordsProcessed = 0;
            int errorCount = 0;
            // Clear Prior Errors
            this._dataErrors.Clear();

            foreach (DataRow rw in this._accessions.Rows)//Erase the uploadstatus column 
                rw["uploadstatus"] = "";
            // Create Data Transfer Object, request and update
            DOIRequest smtaRequest = null;
            DOIUpdate smtaUpdate = null;
            List<string> dataErrors = new List<string>();// List for storing the error catched for pass this errors to de dtgAccession
            int requestId = 0;
            // Walk through each accession id
            foreach (DataRow accession in _accessions.Rows)
            {
                try
                {
                    // Update status column for record
                    accession["uploadstatus"] = (dataErrors.Count() == 0) ? DOIResource.UploadCompleteNoErrors : string.Format(DOIResource.UploadCompleteErrors, dataErrors.Count());
                    requestId = (int)accession["accession_id"];
                    try
                    {
                        ///<summary>
                        ///Dicide Wether be request or update depending of the selection user
                        /// </summary>
                        if (rbtnRequest.Checked == true)
                        {
                            DOIRequest request = new DOIRequest();
                            request.userName = _controller.Configuration.Username;
                            request.Password = _controller.Configuration.Password;
                            smtaRequest = CreateRequestDocument(requestId, request);
                            dataErrors.AddRange(smtaRequest.GetErrors(request, smtaRequest.doiError.ToString()));
                        }
                        if (rbtnUpdate.Checked == true)
                        {
                            DOIUpdate update = new DOIUpdate();
                            update.userName = _controller.Configuration.Username;
                            update.Password = _controller.Configuration.Password;
                            smtaUpdate = CreateUpdateDocument(requestId, update);
                            dataErrors.AddRange(smtaUpdate.GetErrors(update, smtaUpdate.doiError.ToString()));
                        }

                    }
                    catch (Exception ex)
                    {
                        Exception currentEx = ex;
                        while (currentEx.InnerException != null)
                            currentEx = currentEx.InnerException;
                        dataErrors.Add(string.Format(DOIResource.UnexpectedError, currentEx.Message));
                    }


                }
                catch (DOIException se)
                {
                    // If an GLISException occurs, we attempt to provide an informative response to the user
                    switch (se.IssueType)
                    {
                        case GLISErrorType.Authentication:
                            MessageBox.Show(DOIResource.BadCredentialsError, DOIResource.Error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                            break;
                        case GLISErrorType.Configuration:
                            MessageBox.Show(DOIResource.IncorrectServerError, DOIResource.Error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                            break;
                        case GLISErrorType.BadData:
                        case GLISErrorType.Unknown:
                        default:
                            MessageBox.Show(DOIResource.UnexpectedError, DOIResource.Error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                            break;
                    }
                    e.Result = null;
                    return;
                }
                catch (Exception ex)
                {
                    Exception currentEx = ex;
                    while (currentEx.InnerException != null)
                        currentEx = currentEx.InnerException;
                    MessageBox.Show(string.Format(DOIResource.UnexpectedError, currentEx.Message), DOIResource.Error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    e.Result = null;
                    return;
                }

                // Get any errors with object data
                if (dataErrors.Count() == 0)
                {
                    // Transfer the Object
                    smtaResponse = _controller.Send(smtaRequest, smtaUpdate);
                    // If not successful,add returned error message to the errors list
                    if(smtaResponse==null)
                    {
                        dataErrors.Add("There is a not successfull connection, please check the url or the proper connection");
                    }
                    else
                    {
                        if (!smtaResponse.Success)
                        {
                            dataErrors.Add(smtaResponse.Error.ToString());
                        }
                    }
                    
                }
                if (dataErrors.Count() > 0)
                {
                    dataErrors.ForEach(de => _dataErrors.Rows.Add(new object[] { requestId, de }));
                    ++errorCount;
                    // Update status column for recordding
                    accession["uploadstatus"] = (dataErrors.Count() == 0) ? DOIResource.UploadCompleteNoErrors : string.Format(DOIResource.UploadCompleteErrors, dataErrors.Count());
                    dtgAccessions.Rows[_accessions.Rows.IndexOf(accession)].DefaultCellStyle.ForeColor = Color.Red;
                    dataErrors.Clear();
                }
                else
                {
                    // No errors. Store Response in OBJECT REQUEST_ACTION, if is a request is necesari save the DOI
                    if (rbtnUpdate.Checked != true)
                    {
                        if (!RecordGLIS_DOI(requestId, smtaResponse)) ++errorCount;//Calling to record the GLIS DOI just if a request was committed
                    }

                }

                // Update progress in UI
                _bw.ReportProgress(100 * ++recordsProcessed / totalRecords, new UploadStatus() { Processed = recordsProcessed, Errors = errorCount, Total = totalRecords });
                // Return error count for the end of run event
                e.Result = errorCount;
            }
        }
        private void SetStatusDetails(string status = null, IEnumerable<string> errorDetails = null)
        {
            bool hasDetails = errorDetails != null && errorDetails.Count() > 0;

            rtStatusDetails.Clear();
            rtStatusDetails.SelectionAlignment = hasDetails ? HorizontalAlignment.Left : HorizontalAlignment.Center;
            rtStatusDetails.SelectedText = (string.IsNullOrWhiteSpace(status) ? DOIResource.SelectRowForStatus : status) + "\n\n";

            if (hasDetails)
            {
                bool first = true;
                rtStatusDetails.SelectionIndent = 12;
                rtStatusDetails.BulletIndent = 6;
                rtStatusDetails.SelectionBullet = true;
                foreach (var ed in errorDetails)
                {
                    rtStatusDetails.SelectedText = first ? ed : "\n" + ed;
                    first = false;
                }
            }
        }
        /// <summary>
        /// Update the UI with progress as the records are sent
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UploadProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (e.UserState is UploadStatus)
            {
                var status = e.UserState as UploadStatus;
                lblRecordCount.Text = string.Format(DOIResource.Records, status.Processed, status.Total);
                lblErrorCount.Text = string.Format(DOIResource.Errors, status.Errors);
                if (status.Errors > 0)
                    lblErrorCount.ForeColor = Color.Red;
            }
            Cursor.Current = Cursors.WaitCursor;
        }
        /// <summary>
        /// Reset the UI once the transfer is complete
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UploadRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            btnTransfer.Enabled = true;
            btnConfigure.Enabled = true;
            lblUploading.Visible = false;
            pbProgress.Visible = false;

            // Restore cursor
            Cursor.Current = _origCursor;

            if (e.Result == null)
                return;

            // Report Errors to User
            int errors = 0;
            if (e.Result is int)
                errors = (int)e.Result;

            if (errors == 0)
            {
                MessageBox.Show(DOIResource.UploadCompleteNoErrors, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show(string.Format(DOIResource.UploadCompleteErrors, errors), this.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        /// <summary>
        /// Load the order request records to the UI table
        /// </summary>
        /// <param name="parameters"></param>
        private void LoadData(string parameters = "")
        {

            DataSet ds;
            // Get the accession table and bind it to the main form on the General tabpage...
            ds = _controller.SharedUtils.GetWebServiceData("get_glis_data", parameters, 0, 0);

            if (ds.Tables.Contains("get_glis_data"))
            {
                _accessions = ds.Tables["get_glis_data"].Copy();
                if (!_accessions.Columns.Contains("uploadstatus"))
                {
                    _accessions.Columns.Add("uploadstatus");
                }
            }

            dtgAccessions.DataSource = _accessions;

            lblRecordCount.Text = string.Format(DOIResource.Records, 0, _accessions.DefaultView.Count);
            lblErrorCount.Text = string.Format(DOIResource.Errors, 0);
        }

        /// <summary>
        /// Create an DOIRequest object from a GRINGlobal order request record
        /// </summary>
        /// <param name="accessionID">Order Request Id</param>
        /// <returns>SMTA Request object</returns>
        private DOIRequest CreateRequestDocument(int accessionID, DOIRequest request)
        {
            request.doiError = "";
            // Filter the request and accession id item records.
            var orderRow = _accessions.Select(string.Format("accession_id={0}", accessionID)).FirstOrDefault();
            if (orderRow == null)
            {
                return null;
            }

            ///<summary>
            ///Start to fill the data with the dataview fields, this methot asure the mandatory data dosent have empty or null values
            /// </summary>
            request.Location = new GLISLocation();

            //If whatever or both are in the registry
            if ((orderRow["lwiews"].ToString() != "" && orderRow["lwiews"] != null) ||
                 (orderRow["lpid"].ToString() != "" && orderRow["lpid"] != null))
            {
                request.Location.LWIEWS = orderRow["lwiews"].ToString();
                request.Location.LPID = orderRow["lpid"].ToString();
            }//Just if none of lwiews or lpid is present, use the name address and country to obtain the doi code, verifying if all are registry
            else if ((orderRow["lname"].ToString() == "" || orderRow["lname"] == null) ||
                         (orderRow["laddress"].ToString() == "" || orderRow["laddress"] == null) ||
                          (orderRow["lcountry"].ToString() == "" || orderRow["lcountry"] == null))
            {
                request.doiError = "MandatoryMissing";
                return request;
            }
            else//this occurs if all have been registered
            {
                request.Location.lName = orderRow["lname"].ToString();
                request.Location.lAddress = orderRow["laddress"].ToString();
                request.Location.lCountry = orderRow["lcountry"].ToString();
            }
            //The stricted mandatory fields are verifyed in order to avoid whites and nulls. The cropname or genus can be empty but not both
            if ((orderRow["sampleid"].ToString() == "" || orderRow["sampleid"].ToString() == null) ||
                          (orderRow["date"].ToString() == "" || orderRow["date"].ToString() == null) ||
                          (orderRow["method"].ToString() == "" || orderRow["method"].ToString() == null) ||
                          ((orderRow["cropname"].ToString() == "" || orderRow["cropname"] == null) &&
                           (orderRow["genus"].ToString() == "" || orderRow["genus"].ToString() == null)))
            {
                request.doiError = "MandatoryMissing";
                return request;
            }
            if (orderRow["sampledoi"].ToString() != "" && orderRow["sampledoi"] != null)
            {
                request.doiError = "DOIRegisteredAlready";
                return request;
            }
            //Create the next nodes for complete information
            request.sampleDOI = orderRow["sampledoi"].ToString();
            request.sampleID = orderRow["sampleid"].ToString();
            ///<summary>
            ///Check if the string has the correct format to parse datetime
            /// </summary>
            if (!string.IsNullOrWhiteSpace(orderRow["date"].ToString()))
            {

                string sDate = DOI_Transform.formatDate(orderRow["date"].ToString());
                request.Date = sDate;

            }
            request.Method = orderRow["method"].ToString();
            request.Genus = orderRow["genus"].ToString();

            ///<summary>
            ///Generates the list of crop names with the Transform Class with anyList
            /// </summary>
            //Instantiate the cropnames as doicrops
            request.cropNames = new DOICrops();
            //Ingress the list from the string ; split into cropName

            request.cropNames.cropName = DOI_Transform.anyList(DOI_Transform.TitleCase(orderRow["cropName"].ToString()));

            ///<summary>
            ///Create the entire Targets node with its values and keywords
            /// </summary>
            //Create the kws string list for ingress in each target
            var kws = new DOIkws();
            kws.kw = DOI_Transform.anyList(orderRow["tkw"].ToString());

            //Ingress the entire kws node into target 
            var target = new DOITarget();
            target.value = orderRow["tvalue"].ToString();
            target.kws = new List<DOIkws>();
            target.kws.Add(kws);

            //Ingress the entire target node into targets
            request.Targets = new GLISTargets();
            request.Targets.Target = new List<DOITarget>();
            request.Targets.Target.Add(target);

            ///<summary>
            ///Generates the list of doi's for progenitors DOI with the Transform Class with anyList
            /// </summary>
            //Instantiate the progenitor DOI
            request.progDOI = new DOIProgdoi();
            //Create the kws string list for ingress in each target
            request.progDOI.doi = DOI_Transform.anyList(orderRow["progdoi"].ToString());

            request.bioStatus = orderRow["biostatus"].ToString();
            request.Species = orderRow["species"].ToString();
            request.spAuth = orderRow["spauth"].ToString();
            request.subTaxa = orderRow["subtaxa"].ToString();
            request.stAuht = orderRow["stauht"].ToString();

            ///<summary>
            ///Generates the list of names for other sample names with the Transform Class with anyList
            /// </summary>
            //Instantiate the other DOI names
            request.Names = new DOINames();
            //Create the names string list for ingress in the names node
            request.Names.Name = DOI_Transform.anyList(orderRow["nvalue"].ToString());

            ///<summary>
            ///Generates the list with a attribute for each ivalue written with ; split
            /// </summary>
            List<string> lsValue = DOI_Transform.anyList(orderRow["ivalue"].ToString());
            List<string> lsType = DOI_Transform.anyList(orderRow["itype"].ToString());
            int j = 0;
            request.IDS = new DOI_ids();
            request.IDS.ID = new List<AttribID>();
            if (lsValue != null && lsValue != null)
            {
                for (int i = 0; i <= lsValue.Count - 1; i++)
                {
                    request.IDS.ID.Add(new AttribID() { Value = lsValue[i].ToString(), Type = lsType[j].ToString() });

                    if (j < lsType.Count - 1)//the last type attribute is going to be repeated 
                    {
                        j++;
                    }
                }
            }
            request.mlsStatus = Convert.ToInt32(orderRow["mlsstatus"]).ToString();
            request.Hist = orderRow["hist"].ToString();

            ///<summary>Acquisition</summary>
            request.Acqui = new DOIProvider();
            request.Acqui.locationData = new pLocation();
            request.Acqui.locationData.WIEWS = orderRow["pwiews"].ToString();
            request.Acqui.locationData.Name = orderRow["pname"].ToString();
            request.Acqui.locationData.Address = orderRow["paddress"].ToString();
            request.Acqui.locationData.Country = orderRow["pcountry"].ToString();
            request.Acqui.sampleID = orderRow["psampleid"].ToString();
            request.Acqui.Provenance = orderRow["provenance"].ToString();

            ///<summary>Collectors</summary>
            request.Collection = new DOI_Collection();
            request.Collection.Collectors = new Collectors();
            request.Collection.Collectors.Collector = new Collector();
            request.Collection.Collectors.Collector.cWIEWS = orderRow["cwiews"].ToString();
            request.Collection.Collectors.Collector.cName = orderRow["cname"].ToString();
            request.Collection.Collectors.Collector.cAddress = orderRow["caddress"].ToString();
            request.Collection.Collectors.Collector.cCountry = orderRow["ccountry"].ToString();
            request.Collection.cSampleID = orderRow["csampleid"].ToString();
            request.Collection.missID = orderRow["missid"].ToString();
            request.Collection.Site = orderRow["site"].ToString();
            request.Collection.cLat = orderRow["clat"].ToString();
            request.Collection.cLon = orderRow["clon"].ToString();
            request.Collection.Uncert = orderRow["uncert"].ToString();
            request.Collection.Datum = orderRow["datum"].ToString();
            request.Collection.geoRef = orderRow["georef"].ToString();
            request.Collection.Site = orderRow["site"].ToString();
            request.Collection.Elevation = orderRow["elevation"].ToString();
            request.Collection.cDate = orderRow["cdate"].ToString();
            request.Collection.Source = orderRow["source"].ToString();

            ///<summary>Breeders</summary>
            request.Breeding = new DOI_Breeding();
            request.Breeding.Breeders = new Breeders();
            request.Breeding.Breeders.Breeder = new Breeder();
            request.Breeding.Breeders.Breeder.bWIEWS = orderRow["bwiews"].ToString();
            request.Breeding.Breeders.Breeder.bPID = orderRow["bpid"].ToString();
            request.Breeding.Breeders.Breeder.bName = orderRow["bname"].ToString();
            request.Breeding.Breeders.Breeder.bAddress = orderRow["baddress"].ToString();
            request.Breeding.Breeders.Breeder.bCountry = orderRow["bcountry"].ToString();
            request.Breeding.Ancestry = orderRow["ancestry"].ToString();

            //Create file to reviwe what is sending
            //XmlSerializer ser = new XmlSerializer(typeof(DOIRequest));
            //const string FileName = @"C:\prueba1.xml";
            //Stream SaveFileStream = File.Create(FileName);
            //SaveFileStream.Close();

            //TextWriter writer1 = new StreamWriter(FileName);
            //ser.Serialize(writer1,request);
            //writer1.Close();
            return request;
        }

        ///<sumary>
        ///Create the update document in order to update the register recorded in the server already
        /// </sumary>
        private DOIUpdate CreateUpdateDocument(int accessionID, DOIUpdate update)
        {
            update.doiError = "";
            // Filter the request and accession id item records.
            var orderRow = _accessions.Select(string.Format("accession_id={0}", accessionID)).FirstOrDefault();
            if (orderRow == null)
                return null;

            ///<summary>
            ///Start to fill the data with the dataview fields, this method asure the mandatory data dosent have empty or null values
            /// </summary>
            update.Location = new GLISLocation();

            //If whatever or both are in the registry
            if ((orderRow["lwiews"].ToString() != "" && orderRow["lwiews"] != null) ||
                  (orderRow["lpid"].ToString() != "" && orderRow["lpid"] != null))
            {
                update.Location.LWIEWS = orderRow["lwiews"].ToString();
                update.Location.LPID = orderRow["lpid"].ToString();
            }//Just if none of lwiews or lpid are present use the name address and country to obtain the doi code, verifying if all are registry
            else if ((orderRow["lname"].ToString() == "" || orderRow["lname"] == null) ||
                         (orderRow["laddress"].ToString() == "" || orderRow["laddress"] == null) ||
                          (orderRow["lcountry"].ToString() == "" || orderRow["lcountry"] == null))
            {
                update.doiError = "MandatoryMissing";
                return update;
            }
            else
            {
                update.Location.lName = orderRow["lname"].ToString();
                update.Location.lAddress = orderRow["laddress"].ToString();
                update.Location.lCountry = orderRow["lcountry"].ToString();
            }//The stricted mandatory fields are verifyed in order to avoid whites and nulls. The cropname or genus can be empty but not both
            if ((orderRow["sampleid"].ToString() == "" || orderRow["sampleid"].ToString() == null) ||
                          (orderRow["date"].ToString() == "" || orderRow["date"].ToString() == null) ||
                          (orderRow["method"].ToString() == "" || orderRow["method"].ToString() == null) ||
                          ((orderRow["cropname"].ToString() == "" || orderRow["cropname"] == null) &&
                           (orderRow["genus"].ToString() == "" || orderRow["genus"].ToString() == null)))
            {
                update.doiError = "MandatoryMissing";
                return update;
            }
            //Create the next nodes for complete information
            if (orderRow["sampledoi"].ToString() == "" || orderRow["sampledoi"] == null)
            {
                update.doiError = "DOImissingForUpdate";
                return update;
            }

            update.sampleDOI = orderRow["sampledoi"].ToString();
            update.sampleID = orderRow["sampleid"].ToString();
            ///<summary>
            ///Check if the string has the correct format to parse datetime
            /// </summary>
            if (!string.IsNullOrWhiteSpace(orderRow["date"].ToString()))
            {

                string sDate = DOI_Transform.formatDate(orderRow["date"].ToString());
                update.Date = sDate;

            }

            update.Method = orderRow["method"].ToString();
            update.Genus = orderRow["genus"].ToString();

            ///<summary>
            ///Generates the list of crop names with the Transform Class with anyList
            /// </summary>
            //Instantiate the cropnames as doicrops
            update.cropNames = new DOICrops();
            //Ingress the list from the string ; split into cropName

            update.cropNames.cropName = DOI_Transform.anyList(DOI_Transform.TitleCase(orderRow["cropName"].ToString()));

            ///<summary>
            ///Creat the entire Targets node with its values and keywords
            /// </summary>
            //Create the kws string list for ingress in each target
            var kws = new DOIkws();
            kws.kw = DOI_Transform.anyList(orderRow["tkw"].ToString());

            //Ingress the entire kws node into target 
            var target = new DOITarget();
            target.value = orderRow["tvalue"].ToString();
            target.kws = new List<DOIkws>();
            target.kws.Add(kws);

            //Ingress the entire target node into targets
            update.Targets = new GLISTargets();
            update.Targets.Target = new List<DOITarget>();
            update.Targets.Target.Add(target);

            ///<summary>
            ///Generates the list of doi's for progenitors DOI with the Transform Class with anyList
            /// </summary>
            //Instantiate the progenitor DOI
            update.progDOI = new DOIProgdoi();
            //Create the kws string list for ingress in each target
            update.progDOI.doi = DOI_Transform.anyList(orderRow["progdoi"].ToString());

            update.bioStatus = orderRow["biostatus"].ToString();
            update.Species = orderRow["species"].ToString();
            update.spAuth = orderRow["spauth"].ToString();
            update.subTaxa = orderRow["subtaxa"].ToString();
            update.stAuht = orderRow["stauht"].ToString();

            ///<summary>
            ///Generates the list of names for other sample names with the Transform Class with anyList
            /// </summary>
            //Instantiate the other DOI names
            update.Names = new DOINames();
            //Create the names string list for ingress in the names node
            update.Names.Name = DOI_Transform.anyList(orderRow["nvalue"].ToString());

            ///<summary>
            ///Generates the list with a attribute for each ivalue written with ; split
            /// </summary>
            List<string> lsValue = DOI_Transform.anyList(orderRow["ivalue"].ToString());
            List<string> lsType = DOI_Transform.anyList(orderRow["itype"].ToString());
            int j = 0;
            update.IDS = new DOI_ids();
            update.IDS.ID = new List<AttribID>();
            if (lsValue != null && lsValue != null)
            {
                for (int i = 0; i <= lsValue.Count - 1; i++)
                {
                    update.IDS.ID.Add(new AttribID() { Value = lsValue[i].ToString(), Type = lsType[j].ToString() });

                    if (j < lsType.Count - 1)//the last type attribute is going to be repeated 
                    {
                        j++;
                    }
                }
            }
            update.mlsStatus = orderRow["mlsstatus"].ToString();
            update.Hist = orderRow["hist"].ToString();

            ///<summary>Acquisition</summary>
            update.Acqui = new DOIProvider();
            update.Acqui.locationData = new pLocation();
            update.Acqui.locationData.WIEWS = orderRow["pwiews"].ToString();
            update.Acqui.locationData.Name = orderRow["pname"].ToString();
            update.Acqui.locationData.Address = orderRow["paddress"].ToString();
            update.Acqui.locationData.Country = orderRow["pcountry"].ToString();
            update.Acqui.sampleID = orderRow["psampleid"].ToString();
            update.Acqui.Provenance = orderRow["provenance"].ToString();

            ///<summary>Collectors</summary>
            update.Collection = new DOI_Collection();
            update.Collection.Collectors = new Collectors();
            update.Collection.Collectors.Collector = new Collector();
            update.Collection.Collectors.Collector.cWIEWS = orderRow["cwiews"].ToString();
            update.Collection.Collectors.Collector.cName = orderRow["cname"].ToString();
            update.Collection.Collectors.Collector.cAddress = orderRow["caddress"].ToString();
            update.Collection.Collectors.Collector.cCountry = orderRow["ccountry"].ToString();
            update.Collection.cSampleID = orderRow["csampleid"].ToString();
            update.Collection.missID = orderRow["missid"].ToString();
            update.Collection.Site = orderRow["site"].ToString();
            update.Collection.cLat = orderRow["clat"].ToString();
            update.Collection.cLon = orderRow["clon"].ToString();
            update.Collection.Uncert = orderRow["uncert"].ToString();
            update.Collection.Datum = orderRow["datum"].ToString();
            update.Collection.geoRef = orderRow["georef"].ToString();
            update.Collection.Site = orderRow["site"].ToString();
            update.Collection.Elevation = orderRow["elevation"].ToString();
            update.Collection.cDate = orderRow["cdate"].ToString();
            update.Collection.Source = orderRow["source"].ToString();

            ///<summary>Breeders</summary>
            update.Breeding = new DOI_Breeding();
            update.Breeding.Breeders = new Breeders();
            update.Breeding.Breeders.Breeder = new Breeder();
            update.Breeding.Breeders.Breeder.bWIEWS = orderRow["bwiews"].ToString();
            update.Breeding.Breeders.Breeder.bPID = orderRow["bpid"].ToString();
            update.Breeding.Breeders.Breeder.bName = orderRow["bname"].ToString();
            update.Breeding.Breeders.Breeder.bAddress = orderRow["baddress"].ToString();
            update.Breeding.Breeders.Breeder.bCountry = orderRow["bcountry"].ToString();
            update.Breeding.Ancestry = orderRow["ancestry"].ToString();

            //Create file to reviwe what is sending
            //XmlSerializer ser = new XmlSerializer(typeof(DOIUpdate));
            //const string FileName = @"C:\prueba1.xml";
            //Stream SaveFileStream = File.Create(FileName);
            //SaveFileStream.Close();

            //TextWriter writer1 = new StreamWriter(FileName);
            //ser.Serialize(writer1, update);
            //writer1.Close();

            return update;
        }
        /// <summary>
        /// Update Grin Global accession table to record the transfer of the GLIS
        /// </summary>
        /// <param name="accessionRequestId">Order Request Id</param>
        /// <returns>Success</returns>
        private bool RecordGLIS_DOI(int accessionRequestId, DOIResponse smtaResponse)
        {
            bool success = false;
            foreach (DataGridViewRow fila in dtgAccessions.Rows)
            {
                if (fila.Cells["accession_id"].Value.ToString() == accessionRequestId.ToString())
                {
                    if (smtaResponse.DOI == null || smtaResponse.DOI.ToString() == "")
                    {
                        MessageBox.Show("The DOI is missing possibly the server is not responding correctlly");
                        break;
                    }
                    else
                    {
                        if(fila.Cells["sampleid"].ReadOnly == false)
                        {
                            fila.Cells["sampleid"].Value = smtaResponse.sampleID.ToString();
                        }
                        if(fila.Cells["sampledoi"].ReadOnly == false)
                        {
                            fila.Cells["sampledoi"].Value = smtaResponse.DOI.ToString();
                        }
                        if(fila.Cells["genus"].ReadOnly == false)
                        {
                            fila.Cells["genus"].Value = smtaResponse.Genus.ToString();
                        }
                        break;
                    }
                }
            }
            ///<sumary>
            ///The doi code is recorded in the Grin Global accession table
            /// </sumary>
            DataSet ds = _controller.SharedUtils.GetWebServiceData("get_accession", ":accessionid=" + accessionRequestId.ToString(), 0, 0);//Sending the accession request id to the shared utilities
            if (ds.Tables.Contains("get_accession"))
            {
                DataRow dr = ds.Tables["get_accession"].Rows[0];
                dr["doi"] = smtaResponse.DOI.ToString();

                DataSet results = _controller.SharedUtils.SaveWebServiceData(ds);
                if (results.Tables[0].Rows.Count == 0)
                {
                    success = true;
                }
                else
                {
                    foreach (DataRow resultRow in results.Tables[0].Rows)
                    {
                        this._dataErrors.Rows.Add(new[] { accessionRequestId, resultRow["Message"] });
                    }
                }
            }
            return success;
        }

        /// <summary>
        /// Ensure supporting dataviews are present
        /// </summary>
        private bool VerifySetup()
        {
            bool result = true;

            // GLIS Dataviews defined
            var dataviews = _controller.SharedUtils.GetWebServiceData("get_dataview_list", "", 0, 0);
            if (!dataviews.Tables.Contains("get_dataview_list") || dataviews.Tables["get_dataview_list"].Select("dataview_name like 'get_glis_data'").Count() < 1)
            {
                result = false;
            }

            // Provider PID Set
            if (string.IsNullOrWhiteSpace(_controller.SharedUtils.GetAppSettingValue("SMTA-PID-Provider")))
            {
                result = false;
            }

            return result;
        }

        /// <summary>
        /// Helper class providing a mechanism to pass progress info back to the
        /// UI thread.
        /// </summary>
        private class UploadStatus
        {
            /// <summary>
            /// Number of errors
            /// </summary>
            public int Errors { get; set; }

            /// <summary>
            /// Number of records processed
            /// </summary>
            public int Processed { get; set; }

            /// <summary>
            /// Total number of records to process
            /// </summary>
            public int Total { get; set; }
        }

        /// <summary>
        /// When a order request row is select, update the status details text 
        /// with any errors that occurred 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dtgAccessions_SelectionChanged(object sender, EventArgs e)
        {
            if (dtgAccessions.SelectedRows.Count == 0)
                SetStatusDetails();
            else
            {
                string rowStatus = dtgAccessions.SelectedRows[0].Cells["uploadstatus"].Value.ToString();
                if (string.IsNullOrWhiteSpace(rowStatus))
                    SetStatusDetails(DOIResource.UploadDataForStatus);
                else
                {
                    var errors = _dataErrors.Select(string.Format("accession_id = {0}", dtgAccessions.SelectedRows[0].Cells["accession_id"].Value.ToString())).Select(er => er["message"].ToString());
                    SetStatusDetails(rowStatus, errors);
                }
            }
        }
        private void rbtnTransfer_CheckedChanged(object sender, EventArgs e)
        {
            if(btnTransfer.Enabled==true)
            {
                btnTransfer.Enabled = false;
            }else { btnTransfer.Enabled = true; }
            
        }
    }
}
