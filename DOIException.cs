﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DOIManagerWizard
{

    /// <summary>
    /// Enumeration of expected error types
    /// </summary>
    public enum GLISErrorType : byte
    {
        Unknown = 0,
        Authentication,
        Configuration,
        BadData
    }

    /// <summary>
    /// GLIS Exception simplifying error responses to sending GLIS documents,
    /// and translating to an issue type enumeration
    /// </summary>
    public class DOIException : Exception
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="message">Error message</param>
        public DOIException(string message) : base(message)
        {
            switch (message)
            {
                case "Access denied":
                    this.IssueType = GLISErrorType.Authentication;
                    break;
                case "Request sent over HTTP instead of HTTPS":
                    this.IssueType = GLISErrorType.Configuration;
                    break;
                case "Empty XML document":
                    this.IssueType = GLISErrorType.BadData;
                    break;
                default:
                    this.IssueType = GLISErrorType.Unknown;
                    break;
            }
        }

        /// <summary>
        /// Issue type
        /// </summary>
        public GLISErrorType IssueType { get; set; }
    }
}
