﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DOIManagerWizard
{
    /// <summary>
    /// Configuration settings form
    /// </summary>
    public partial class EasyDOIConfigForm : Form
    {
        /// <summary>
        /// GLIS Controller
        /// </summary>
        private eDOIController _controller;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="controller">GLIS Controller</param>
        public EasyDOIConfigForm(eDOIController controller)
        {
            this._controller = controller;
            InitializeComponent();
        }

        private void EasyDOIConfigForm_Load(object sender, EventArgs e)
        {
            // Update Controls based on Languages
            if (this.components != null && this.components.Components != null) _controller.SharedUtils.UpdateComponents(this.components.Components, this.Name);
            if (this.Controls != null) _controller.SharedUtils.UpdateControls(this.Controls, this.Name);

            // Load current values
            txtServerAddress.Text = _controller.Configuration.ServerUri.ToString();
            txtUsername.Text = _controller.Configuration.Username;
            txtPassword.Text = _controller.Configuration.Password;
        }
        /// <summary>
        /// Cancel button event handler. Close form without making changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Save button event handler. Persist changes to storage and close dialog
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            if (!CheckServerValid(txtServerAddress.Text))
            {
                MessageBox.Show(DOIResource.ServerInvalid_Message, DOIResource.ServerInvalid_Caption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            _controller.Configuration.ServerUri = new Uri(txtServerAddress.Text);
            _controller.Configuration.Username = txtUsername.Text;
            _controller.Configuration.Password = txtPassword.Text;

            this.Close();
        }

        private void llblPlantTreatyLink_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            ProcessStartInfo sInfo = new ProcessStartInfo("https://mls.planttreaty.org/");
            Process.Start(sInfo);
        }

        /// <summary>
        /// Helper function to ensure server address is a valid Uri
        /// </summary>
        /// <param name="serverAddress">Server address</param>
        /// <returns>Inidicates whether it is valid</returns>
        private bool CheckServerValid(string serverAddress)
        {
            Uri uri = null;
            return Uri.TryCreate(serverAddress, UriKind.Absolute, out uri);
        }
    }
}
