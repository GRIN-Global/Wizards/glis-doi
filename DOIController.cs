﻿using DOIManagerWizard.Model;
using GRINGlobal.Client.Common;
using System;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Windows.Forms;
using System.Xml.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text.RegularExpressions;
using System.Security.AccessControl;

namespace DOIManagerWizard
{
    /// <summary>
    /// GLIS Controller helps orchestrate the interaction between the curator tool 
    /// and the GLIS server 
    /// </summary>
    public class DOIController : eDOIController
    {
        private SharedUtils _sharedUtils;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="sharedUtils">Curator Tool SharedUtils</param>
        public DOIController(SharedUtils sharedUtils)
        {
            this._sharedUtils = sharedUtils;
            this.Configuration = new Configuration(string.Format("{0}\\GRIN-Global\\Curator Tool\\glis_{1}.txt", System.Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), _sharedUtils.Username));
        }

        /// <summary>
        /// Launch configuration dialog
        /// </summary>
        public void Configure()
        {
            EasyDOIConfigForm cf = new EasyDOIConfigForm(this);
            cf.StartPosition = FormStartPosition.CenterParent;
            cf.ShowDialog();
        }

        /// <summary>
        /// Send the GLIS request to the GLIS server
        /// </summary>
        /// <param name="request">GLIS request</param>
        /// <returns>SMTA response object</returns>
        public DOIResponse Send(DOIRequest request, DOIUpdate update)
        {

            // Serialize the Request Object to XML
           byte[] body = null;
           using (MemoryStream stream = new MemoryStream())
            {
                using (StreamWriter writer = new StreamWriter(stream, Encoding.UTF8))//Writes the stream with the specified encoding
                {
                    if (request == null)
                    {
                        XmlSerializer requestSerializer = new XmlSerializer(typeof(DOIUpdate));
                        requestSerializer.Serialize(writer, update);
                    }
                    else {
                        XmlSerializer requestSerializer = new XmlSerializer(typeof(DOIRequest));
                        requestSerializer.Serialize(writer, request); }
                    
                    body = stream.ToArray();
                }
            }
           ///<summary>
           ///Set the security protocol accepted for Linux Server with PHP, for some reason use the same WCF TLS 1.2 protocol 
           /// </summary>
            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
            
            // Strip "Byte Order Mark" from array so '<' is the first character
            byte[] arrSkipBOM = DOI_Transform.arrSkipBOM(body);
           
            ///<summary>
            ///Creat an http web request instance with the configuration server uri
            /// </summary>
            var httpClient_Setting = (HttpWebRequest)WebRequest.Create(Configuration.ServerUri);

            ///<summary>
            ///Set all the http settings required
            ///</summary>
            httpClient_Setting.ProtocolVersion = HttpVersion.Version11;
            httpClient_Setting.Method = "POST";
            Encoding.UTF8.GetString(arrSkipBOM.ToArray());
            httpClient_Setting.ContentLength = arrSkipBOM.Length;
            httpClient_Setting.ContentType = "application/xml";

            ///<summary>
            ///Create a response instance rutine to recive the data and catch the errors
            ///</summary>
            var responseValue = string.Empty;
            HttpWebResponse httpResponse = null;
            DOIResponse resp = null;
            string sResp;
            try
            {
                ///<summary>
                ///Create a stream instance to passs the data
                ///</summary>
                Stream sData_Request = httpClient_Setting.GetRequestStream();
                sData_Request.Write(arrSkipBOM, 0, arrSkipBOM.Length);
                sData_Request.Close();
            
                ///<sumary>
                ///Instance the reponse from the server
                ///</sumary>
                httpResponse = (HttpWebResponse)httpClient_Setting.GetResponse();
                ///<summary>
                ///Create a stream instance to read the data
                ///</summary>
                Stream sData_Response = httpResponse.GetResponseStream();

                ///<summary>
                ///Decode the stream in a bytes array
                /// </summary>
                byte[] msContent_Response = DOI_Transform.arrSkipBOM(DOI_Transform.StreamTo_arrByte(sData_Response));
                sData_Response.Close();

                //Create file and write or rewrite if exist with the GLIS response message
                //File.Create(@"C:\_Response.xml").Dispose();
                //File.WriteAllBytes(@"C:\_Response.xml", msContent_Response.ToArray());
                
                ///<summary>
                ///Check the server response and the content if is not empty
                /// </summary>
                if (((HttpWebResponse)httpResponse).StatusDescription!= "OK"|| (msContent_Response.Length <= 0 && msContent_Response.ToArray()[0] != '<'))
                {
                    // HTTP transaction failed, so throw an exception
                    throw new DOIException(msContent_Response.ToString());
                }
                ///<summary>
                ///Encoding the array again, with this make sure the format, BOM and header could be read correctly at the deserialization time
                /// </summary>
                string content = UnicodeEncoding.UTF8.GetString(msContent_Response.ToArray());

                //// Deserialize the Response
                XmlSerializer responseSerializer = new XmlSerializer(typeof(DOIResponse));
                resp = (DOIResponse)responseSerializer.Deserialize(new StringReader(content));
                resp.Result = ((HttpWebResponse)httpResponse).StatusDescription;

                return resp;
            }
            catch (WebException ex)
            {
                ///<summary>
                ///Is possible the response has 3 protocol error must be catched, 400 Bad Request, 401 Unauthorized and 503 Service Unavailable 
                /// </summary>
                if (ex.Status == WebExceptionStatus.ProtocolError)
                {
                    Stream exResp = ex.Response.GetResponseStream();//Extending the response to the exception variable
                    StreamReader exRespRead = new StreamReader(exResp);
                    sResp = exRespRead.ReadToEnd();
                    HttpStatusCode status = ((HttpWebResponse)ex.Response).StatusCode;
                    if (status.ToString() == "BadRequest")
                    {

                        XmlSerializer responseSerializer = new XmlSerializer(typeof(DOIResponse));
                        resp = (DOIResponse)responseSerializer.Deserialize(new StringReader(sResp));
                        //The pattern says whatever nummber between 0 and 9, whit just 2 digits, one period, 4 digits from 0 to 9, just one slash and ending 4 characters number or word 
                        string patt = @"(10)\.[\s\S]{2,}[/][\s\S][^\]]{2,}";//This pattern localize the DOI code in all the character chain, no matter where it be
                        //Matching the pattern in all the text.
                        Match doi = Regex.Match(sResp, patt);
                        string sdoi = doi.ToString();//Convert the stracted text to string, in order to stract the filtered fully DOI code 
                        int endIndex = sdoi.LastIndexOf("]");//Takes the character to delimit the DOI code length
                        sdoi = sdoi.Substring(0, endIndex);//Stract filtered fully the DOI code
                        if (sResp.Contains(sdoi) && sdoi != null && sdoi != "")//If the response has a format structure doi, in any part of the content, then will save this in resp.doi
                        {
                            resp.Result = "OK";//The result is setted as OK
                            resp.DOI = sdoi;//The doi found is setted in the resp instance class
                            resp.Error = null;
                        }else if(update!=null && resp.Error==null)//Is a good response, if aint update and there is not errors
                        {
                            resp.Result = "OK";
                            return resp;
                        }else { return resp; }
                        
                    }
                    if (status.ToString() == "ServiceUnavailable")
                    {
                        resp.Error += " due server overloaded because unexpected traffic peak";
                        return resp;
                    }
                    if(status.ToString()=="Unauthorized")
                    {
                        resp.Error += " The user and/or password is incorrect, the server access was denyed";
                        return resp;
                    }else if(resp.Result !="OK")
                    {
                        DirectoryInfo di = new DirectoryInfo("GlisResp");
                        try
                        {
                            // Determine whether the directory exists.
                            if (di.Exists)
                            {
                                File.Create(@"GlisResp\_Response.xml").Dispose();
                                File.WriteAllText(@"GlisResp\_Response.xml", sResp);
                            }
                            else
                            {
                                // Try to create the directory.
                                di.Create();
                                File.Create(@"GlisResp\_Response.xml").Dispose();
                                File.WriteAllText(@"GlisResp\_Response.xml", sResp);
                            }

                        }
                        catch (Exception e)
                        {
                            resp.Error = e.ToString();
                        }
                        resp.Error = "There is somthing very wrong with the server, please contact to the Administrator and send him the TargetDir GlisResp\\_Response.xml file";
                    }
                    return resp;
                }
                else
                {
                    if (httpResponse != null)
                    {
                        var message = String.Format("Response failed. Received HTTP {0}", httpResponse.StatusCode);

                        throw new ApplicationException(message);
                    }
                    else
                    {
                        MessageBox.Show("There is not connection or the server url does not exist. "+ex.Message.ToString(), "Without Response", MessageBoxButtons.OK,MessageBoxIcon.Asterisk);
                        return resp;
                    }
                    
                }
            }
        }
        /// <summary>
        /// GRINGlobal Client Shared Utilities
        /// </summary>
        public SharedUtils SharedUtils
        {
            get
            {
                return _sharedUtils;
            }
        }
        /// <summary>
        /// SMTA configuration
        /// </summary>
        public Configuration Configuration { get; private set; }
        /// <summary>
        /// Compress the byte stream using GZip
        /// </summary>
        /// <param name="data">Uncompressed data</param>
        /// <returns>Compressed data</returns>
        private byte[] Compress(byte[] data)
        {
            if (data == null)
                throw new ArgumentNullException("data must be non-null");

            using (var compressIntoMs = new MemoryStream())
            {
                using (var gzs = new BufferedStream(new GZipStream(compressIntoMs,
                CompressionMode.Compress), 64 * 1024)) // 64k buffer
                {
                    gzs.Write(data, 0, data.Length);
                }
                return compressIntoMs.ToArray();
            }
        }
    }
}
