﻿using DOIManagerWizard.Model;
using GRINGlobal.Client.Common;
using System.Threading.Tasks;

namespace DOIManagerWizard
{
    /// <summary>
    /// GLIS Controller Interface
    /// </summary>
    public interface eDOIController
    {
        /// <summary>
        /// GLIS Connection Configuration
        /// </summary>
        Configuration Configuration { get; }

        /// <summary>
        /// Launch configuration dialog
        /// </summary>
        void Configure();

        /// <summary>
        /// Send a request to SMTA
        /// </summary>
        /// <param name="request">SMTA request</param>
        /// <returns></returns>
        DOIResponse Send(DOIRequest request, DOIUpdate update);

        /// <summary>
        /// Reference to Curator Tool Utils class
        /// </summary>
        SharedUtils SharedUtils { get; }
    }
}
