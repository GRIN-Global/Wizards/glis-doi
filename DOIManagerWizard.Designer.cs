﻿namespace DOIManagerWizard
{
    partial class DOIManagerWizard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DOIManagerWizard));
            this.rbtnTransfer = new System.Windows.Forms.RadioButton();
            this.rbtnUpdate = new System.Windows.Forms.RadioButton();
            this.rbtnRequest = new System.Windows.Forms.RadioButton();
            this.gbStatusDetails = new System.Windows.Forms.GroupBox();
            this.rtStatusDetails = new System.Windows.Forms.RichTextBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnTransfer = new System.Windows.Forms.Button();
            this.dtgAccessions = new System.Windows.Forms.DataGridView();
            this.btnConfigure = new System.Windows.Forms.Button();
            this.lblRecordCount = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblErrorCount = new System.Windows.Forms.ToolStripStatusLabel();
            this.pbProgress = new System.Windows.Forms.ToolStripProgressBar();
            this.lblUploading = new System.Windows.Forms.ToolStripStatusLabel();
            this.ssStatusStrip = new System.Windows.Forms.StatusStrip();
            this.gbStatusDetails.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgAccessions)).BeginInit();
            this.ssStatusStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // rbtnTransfer
            // 
            this.rbtnTransfer.AutoSize = true;
            this.rbtnTransfer.Location = new System.Drawing.Point(234, 7);
            this.rbtnTransfer.Name = "rbtnTransfer";
            this.rbtnTransfer.Size = new System.Drawing.Size(111, 17);
            this.rbtnTransfer.TabIndex = 408;
            this.rbtnTransfer.Text = "Transfer Inventory";
            this.rbtnTransfer.UseVisualStyleBackColor = true;
            this.rbtnTransfer.CheckedChanged += new System.EventHandler(this.rbtnTransfer_CheckedChanged);
            // 
            // rbtnUpdate
            // 
            this.rbtnUpdate.AutoSize = true;
            this.rbtnUpdate.Location = new System.Drawing.Point(128, 7);
            this.rbtnUpdate.Name = "rbtnUpdate";
            this.rbtnUpdate.Size = new System.Drawing.Size(81, 17);
            this.rbtnUpdate.TabIndex = 409;
            this.rbtnUpdate.Text = "Update Info";
            this.rbtnUpdate.UseVisualStyleBackColor = true;
            // 
            // rbtnRequest
            // 
            this.rbtnRequest.AutoSize = true;
            this.rbtnRequest.Checked = true;
            this.rbtnRequest.Location = new System.Drawing.Point(12, 7);
            this.rbtnRequest.Name = "rbtnRequest";
            this.rbtnRequest.Size = new System.Drawing.Size(87, 17);
            this.rbtnRequest.TabIndex = 410;
            this.rbtnRequest.TabStop = true;
            this.rbtnRequest.Text = "Request DOI";
            this.rbtnRequest.UseVisualStyleBackColor = true;
            // 
            // gbStatusDetails
            // 
            this.gbStatusDetails.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbStatusDetails.Controls.Add(this.rtStatusDetails);
            this.gbStatusDetails.Location = new System.Drawing.Point(594, 6);
            this.gbStatusDetails.Name = "gbStatusDetails";
            this.gbStatusDetails.Size = new System.Drawing.Size(208, 349);
            this.gbStatusDetails.TabIndex = 405;
            this.gbStatusDetails.TabStop = false;
            this.gbStatusDetails.Text = "Status Details";
            // 
            // rtStatusDetails
            // 
            this.rtStatusDetails.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rtStatusDetails.BackColor = System.Drawing.SystemColors.Control;
            this.rtStatusDetails.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtStatusDetails.BulletIndent = 1;
            this.rtStatusDetails.CausesValidation = false;
            this.rtStatusDetails.Location = new System.Drawing.Point(17, 66);
            this.rtStatusDetails.Name = "rtStatusDetails";
            this.rtStatusDetails.Size = new System.Drawing.Size(177, 263);
            this.rtStatusDetails.TabIndex = 1;
            this.rtStatusDetails.Text = "";
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnClose.Location = new System.Drawing.Point(701, 361);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 403;
            this.btnClose.Text = "&Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnTransfer
            // 
            this.btnTransfer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTransfer.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnTransfer.Location = new System.Drawing.Point(611, 361);
            this.btnTransfer.Name = "btnTransfer";
            this.btnTransfer.Size = new System.Drawing.Size(75, 23);
            this.btnTransfer.TabIndex = 402;
            this.btnTransfer.Text = "&Commit Data";
            this.btnTransfer.UseVisualStyleBackColor = true;
            this.btnTransfer.Click += new System.EventHandler(this.btnTransfer_Click);
            // 
            // dtgAccessions
            // 
            this.dtgAccessions.AllowUserToAddRows = false;
            this.dtgAccessions.AllowUserToDeleteRows = false;
            this.dtgAccessions.AllowUserToResizeRows = false;
            this.dtgAccessions.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgAccessions.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgAccessions.Location = new System.Drawing.Point(12, 45);
            this.dtgAccessions.MultiSelect = false;
            this.dtgAccessions.Name = "dtgAccessions";
            this.dtgAccessions.RowHeadersVisible = false;
            this.dtgAccessions.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtgAccessions.ShowCellErrors = false;
            this.dtgAccessions.ShowCellToolTips = false;
            this.dtgAccessions.ShowEditingIcon = false;
            this.dtgAccessions.ShowRowErrors = false;
            this.dtgAccessions.Size = new System.Drawing.Size(563, 310);
            this.dtgAccessions.TabIndex = 406;
            this.dtgAccessions.SelectionChanged += new System.EventHandler(this.dtgAccessions_SelectionChanged);
            // 
            // btnConfigure
            // 
            this.btnConfigure.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnConfigure.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnConfigure.Location = new System.Drawing.Point(12, 361);
            this.btnConfigure.Name = "btnConfigure";
            this.btnConfigure.Size = new System.Drawing.Size(75, 23);
            this.btnConfigure.TabIndex = 407;
            this.btnConfigure.Text = "&Configure...";
            this.btnConfigure.UseVisualStyleBackColor = true;
            this.btnConfigure.Click += new System.EventHandler(this.btnConfigure_Click);
            // 
            // lblRecordCount
            // 
            this.lblRecordCount.Name = "lblRecordCount";
            this.lblRecordCount.Size = new System.Drawing.Size(61, 17);
            this.lblRecordCount.Text = "Records: 0";
            // 
            // lblErrorCount
            // 
            this.lblErrorCount.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.lblErrorCount.Margin = new System.Windows.Forms.Padding(50, 3, 0, 2);
            this.lblErrorCount.Name = "lblErrorCount";
            this.lblErrorCount.Size = new System.Drawing.Size(49, 17);
            this.lblErrorCount.Text = "Errors: 0";
            this.lblErrorCount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // pbProgress
            // 
            this.pbProgress.AutoSize = false;
            this.pbProgress.Name = "pbProgress";
            this.pbProgress.Size = new System.Drawing.Size(135, 16);
            this.pbProgress.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.pbProgress.Visible = false;
            // 
            // lblUploading
            // 
            this.lblUploading.AutoSize = false;
            this.lblUploading.Margin = new System.Windows.Forms.Padding(50, 3, 0, 2);
            this.lblUploading.Name = "lblUploading";
            this.lblUploading.Size = new System.Drawing.Size(118, 17);
            this.lblUploading.Text = "Uploading Data:";
            this.lblUploading.Visible = false;
            // 
            // ssStatusStrip
            // 
            this.ssStatusStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.ssStatusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblRecordCount,
            this.lblErrorCount,
            this.lblUploading,
            this.pbProgress});
            this.ssStatusStrip.Location = new System.Drawing.Point(0, 397);
            this.ssStatusStrip.Name = "ssStatusStrip";
            this.ssStatusStrip.Padding = new System.Windows.Forms.Padding(1, 0, 10, 0);
            this.ssStatusStrip.Size = new System.Drawing.Size(814, 22);
            this.ssStatusStrip.TabIndex = 404;
            this.ssStatusStrip.Text = "statusStrip1";
            // 
            // DOIManagerWizard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(814, 419);
            this.Controls.Add(this.rbtnTransfer);
            this.Controls.Add(this.rbtnUpdate);
            this.Controls.Add(this.rbtnRequest);
            this.Controls.Add(this.gbStatusDetails);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnTransfer);
            this.Controls.Add(this.dtgAccessions);
            this.Controls.Add(this.btnConfigure);
            this.Controls.Add(this.ssStatusStrip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "DOIManagerWizard";
            this.Text = "DOI Manager Wizard";
            this.Load += new System.EventHandler(this.DOIManagerWizard_Load);
            this.gbStatusDetails.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgAccessions)).EndInit();
            this.ssStatusStrip.ResumeLayout(false);
            this.ssStatusStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton rbtnTransfer;
        private System.Windows.Forms.RadioButton rbtnUpdate;
        private System.Windows.Forms.RadioButton rbtnRequest;
        private System.Windows.Forms.GroupBox gbStatusDetails;
        private System.Windows.Forms.RichTextBox rtStatusDetails;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnTransfer;
        private System.Windows.Forms.DataGridView dtgAccessions;
        private System.Windows.Forms.Button btnConfigure;
        private System.Windows.Forms.ToolStripStatusLabel lblRecordCount;
        private System.Windows.Forms.ToolStripStatusLabel lblErrorCount;
        private System.Windows.Forms.ToolStripProgressBar pbProgress;
        private System.Windows.Forms.ToolStripStatusLabel lblUploading;
        private System.Windows.Forms.StatusStrip ssStatusStrip;
    }
}