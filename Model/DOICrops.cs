﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace DOIManagerWizard.Model
{
    
    [XmlRoot("cropnames")]
    public class DOICrops
    {
        [XmlElement("name")]
        public List<string> cropName { get; set; }
    }
}
