﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace DOIManagerWizard.Model
{
    public class GLISLocation
    {
        [XmlElement("wiews")]
        public string LWIEWS { get; set; }
        [XmlElement("pid")]
        public string LPID { get; set; }
        [XmlElement("name")]
        public string lName { get; set; }
        [XmlElement("address")]
        public string lAddress { get; set; }
        [XmlElement("country")]
        public string lCountry { get; set; }
    }
}
