﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DOIManagerWizard.Model
{
    public class DOI_Transform
    {
        /// <summary>
        /// Take Off the BOM(Byte Order Mark) to perform the desarialization
        /// </summary>
        /// <param name="body"></param>
        /// <returns></returns>
        public static byte[] arrSkipBOM(byte[] body)
        {
           int gtIndex = -1;
           for (int i = 0; i < body.Length && gtIndex < 0; i++)
           {
               if (body[i] == 60) // Check for '<' character
               {
                   gtIndex = i;
               }
           }
           if (gtIndex > 0)
           {
               body = body.Skip(gtIndex).ToArray();
               return body;
           }
           else return body;
        }
        /// <summary>
        /// Decode a tream data to byte arrays
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static byte[] StreamTo_arrByte(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }
        ///<summary>
        ///Fromatting the string to datetime
        /// </summary>
        public static string formatDate(string nFormatDate)
        {
            DateTime isDate;
            if (DateTime.TryParseExact(nFormatDate, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out isDate))
            {
                nFormatDate = isDate.ToString();
                return nFormatDate;
            }
            else if (DateTime.TryParseExact(nFormatDate, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out isDate))
            {
                return DateTime.ParseExact(nFormatDate, "yyyyMMdd", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd");
            }
            else
            {
                int counter = 0;
                string formatedDate = "";
                string _Date=nFormatDate.Replace("-", "");
                foreach (var letter in _Date)
                {
                    counter++;
                    if (counter < 5)
                    {
                        formatedDate += letter.ToString();
                    }
                    else if (counter >= 5 && counter < 8)
                    {
                        formatedDate += "-" + letter.ToString();
                    }
                    else if (counter >= 8 && counter <= 10)
                    {
                        formatedDate += "-" + letter.ToString();
                    }
                    else { return null; }
                   
                }
                return formatedDate;
            }
        }
        ///<summary>
        ///Create a list from any node string parameter (Char;Char1;Char2) as the names of xml node
        /// </summary>
        public static List<string> anyList(string itemNames)
        {
            List<string> lista = new List<string> { };
            
            string[] internalList = itemNames.Split(';');

            if (itemNames!="" && internalList.Count() >= 1 && internalList!=null)
            {
                foreach (var item in internalList)
                {
                    lista.Add(item.ToString());
                }
                if (lista.Count > 0 && lista != null)
                {
                    return lista;
                }
                else
                {
                    return null;
                }

            } else { return null; }
            
        }
        public static string TitleCase(string data_toTitel)
        {
            TextInfo myTI = new CultureInfo("en-US", false).TextInfo;
            data_toTitel = myTI.ToLower(data_toTitel);
            return myTI.ToTitleCase(data_toTitel);
        }
       
    }
}
