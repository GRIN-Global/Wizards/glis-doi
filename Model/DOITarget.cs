﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace DOIManagerWizard.Model
{
    [XmlRoot("target")]
    public class DOITarget
    {
        [XmlElement("value")]
        public string value { get; set; }
        [XmlElement("kws")]
        public List<DOIkws> kws { get; set; }

    }
}
