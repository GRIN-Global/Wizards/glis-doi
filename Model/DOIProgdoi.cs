﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace DOIManagerWizard.Model
{
    public class DOIProgdoi
    {
        [XmlElement("doi")]
        public List<string> doi { get; set; }
    }
}
