﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace DOIManagerWizard.Model
{
    [XmlRoot("targets")]
    public class GLISTargets
    {
        [XmlElement("target")]
        public List<DOITarget> Target { get; set; }
    }
}
