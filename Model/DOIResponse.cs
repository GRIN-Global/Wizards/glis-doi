﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using DOIManagerWizard;

namespace DOIManagerWizard.Model
{
    /// <summary>
    /// SMTA Response object
    /// </summary>
    [XmlRoot("response", IsNullable =false)]
    public class DOIResponse
    {
        /// <summary>
        /// Symbol of the SMTA sent in the request
        /// </summary>
        [XmlElement("sampleid")]
        public string sampleID { get; set; }

        /// <summary>
        /// Provider PID
        /// </summary>
        [XmlElement("genus")]
        public string Genus { get; set; }

        /// <summary>
        /// The DOI requiried
        /// </summary>
        [XmlElement("doi")]
        public string DOI { get; set; }
        /// <summary>
        /// Result of the tranfer. OK for success or KO for failure
        /// </summary>
        [XmlElement("result")]
        public string Result { get; set; }

        /// <summary>
        /// Array of errors
        /// </summary>
        /*[XmlElement("error")]
        public List<SMTAError> Error { get; set; }*/
        [XmlElement("error")]
        public string Error { get; set; }
        /// <summary>
        /// Indicates if successful
        /// </summary>
        public bool Success
        {
            get
            {
                return Result == "OK";
            }
        }
    }
}
