﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace DOIManagerWizard.Model
{
    [XmlRoot("acquisition")]
    public class DOIProvider
    {
        [XmlElement("provider")]
        public pLocation locationData { get; set; }
        [XmlElement("sampleid")]
        public string sampleID { get; set; }
        [XmlElement("provenance")]
        public string Provenance { get; set; }
    }
    [XmlRoot("provider")]
    public class pLocation
    {
        [XmlElement("wiews")]
        public string WIEWS { get; set; }
        [XmlElement("pid")]
        public string PID { get; set; }
        [XmlElement("name")]
        public string Name { get; set; }
        [XmlElement("address")]
        public string Address { get; set; }
        [XmlElement("country")]
        public string Country { get; set; }
    }
}
