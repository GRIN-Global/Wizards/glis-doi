﻿using DOIManagerWizard;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace DOIManagerWizard.Model
{
    /// <summary>
    /// Main GLIS Update object
    /// </summary>
    [XmlRoot("update", IsNullable = false)]
    public class DOIUpdate
    {
        /// <summary>
        /// Adding atributes to the root
        /// </summary>
        [XmlAttribute("username")]
        public string userName { get; set; }
        [XmlAttribute("password")]
        public string Password { get; set; }


        [XmlElement("location")]
        public GLISLocation Location { get; set; }

        [XmlElement("sampledoi")]
        public string sampleDOI { get; set; }
        [XmlElement("sampleid")]
        public string sampleID { get; set; }

        [XmlElement("date")]
        public string Date { get; set; }

        [XmlElement("method")]
        public string Method { get; set; }
        [XmlElement("genus")]
        public string Genus { get; set; }

        [XmlElement("cropnames")]
        public DOICrops cropNames { get; set; }

        [XmlElement("targets")]
        public GLISTargets Targets { get; set; }

        [XmlElement("progdoi")]
        public DOIProgdoi progDOI { get; set; }

        [XmlElement("biostatus")]
        public string bioStatus { get; set; }
        [XmlElement("species")]
        public string Species { get; set; }

        [XmlElement("spauth")]
        public string spAuth { get; set; }
        [XmlElement("subtaxa")]
        public string subTaxa { get; set; }
        [XmlElement("stauth")]
        public string stAuht { get; set; }

        [XmlElement("names")]
        public DOINames Names { get; set; }

        [XmlElement("ids")]
        public DOI_ids IDS { get; set; }

        [XmlElement("mlsstatus")]
        public string mlsStatus { get; set; }
        [XmlElement("historical")]
        public string Hist { get; set; }

        [XmlElement("acquisition")]
        public DOIProvider Acqui { get; set; }

        [XmlElement("collection")]
        public DOI_Collection Collection { get; set; }

        [XmlElement("breeding")]
        public DOI_Breeding Breeding { get; set; }

        [XmlIgnore]
        public string doiError { get; set; }
        /// <summary>
        /// Get errors with the request data before sending
        /// </summary>
        /// <returns></returns>
        public IEnumerable<string> GetErrors(DOIUpdate update, string dato)
        {
            List<string> errors = new List<string>();
            if (dato != "")
            {
                if (dato == "MandatoryMissing")
                    errors.Add(DOIResource.MandatoryMissing);
                if (dato == "DOImissingForUpdate")
                    errors.Add(DOIResource.DOImissingForUpdate);
            }
            return errors;
        }
    }
}
