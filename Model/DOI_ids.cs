﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace DOIManagerWizard.Model
{
    [XmlRoot("ids")]
    public class DOI_ids
    {
        [XmlElement("id")]
        public List<AttribID> ID { get; set; }
    }
    [XmlRoot("id")]
    public class AttribID
    {
        [XmlAttribute("type")]
        public string Type { get; set; }
        [XmlText]
        public string Value { get; set; }
    }
}
