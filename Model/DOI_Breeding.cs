﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace DOIManagerWizard.Model
{
    public class DOI_Breeding
    {
        [XmlElement("breeders")]
        public Breeders Breeders { get; set; }
        [XmlElement("ancestry")]
        public string Ancestry { get; set; }
    }
    public class Breeders
    {
        [XmlElement("breeder")]
        public Breeder Breeder { get; set; }
    }
    public class Breeder
    {
        [XmlElement("wiews")]
        public string bWIEWS { get; set; }
        [XmlElement("pid")]
        public string bPID { get; set; }
        [XmlElement("name")]
        public string bName { get; set; }
        [XmlElement("address")]
        public string bAddress { get; set; }
        [XmlElement("country")]
        public string bCountry { get; set; }
    }
}
