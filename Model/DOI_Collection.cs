﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace DOIManagerWizard.Model
{
    public class DOI_Collection
    {
        [XmlElement("collectors")]
        public Collectors Collectors { get; set; }
        [XmlElement("sampleid")]
        public string cSampleID { get; set; }
        [XmlElement("missid")]
        public string missID { get; set; }
        [XmlElement("site")]
        public string Site { get; set; }
        [XmlElement("lat")]
        public string cLat { get; set; }
        [XmlElement("lon")]
        public string cLon { get; set; }
        [XmlElement("uncert")]
        public string Uncert { get; set; }
        [XmlElement("datum")]
        public string Datum { get; set; }
        [XmlElement("georef")]
        public string geoRef { get; set; }
        [XmlElement("elevation")]
        public string Elevation { get; set; }
        [XmlElement("date")]
        public string cDate { get; set; }
        [XmlElement("source")]
        public string Source { get; set; }
    }
    public class Collectors
    {
        [XmlElement("collector")]
        public Collector Collector { get; set; }
    }
    public class Collector
    {
        [XmlElement("wiews")]
        public string cWIEWS { get; set; }
        [XmlElement("pid")]
        public string cPID { get; set; }
        [XmlElement("name")]
        public string cName { get; set; }
        [XmlElement("address")]
        public string cAddress { get; set; }
        [XmlElement("country")]
        public string cCountry { get; set; }
    }
}
