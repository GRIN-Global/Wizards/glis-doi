﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace DOIManagerWizard.Model
{
    [XmlRoot("kws")]
    public class DOIkws
    {
        [XmlElement("kw")]
        public List<string> kw { get; set; }
    }
}
