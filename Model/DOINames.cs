﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace DOIManagerWizard.Model
{
    [XmlRoot("names")]
    public class DOINames
    {
        [XmlElement("name")]
        public List<string> Name { get; set; }
    }
}
